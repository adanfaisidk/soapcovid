//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.2.11
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source.
// Généré le : 2021.02.16 à 01:14:48 PM UTC
//


package com.mycompany.myapp.wsdl;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Classe Java pour anonymous complex type.
 *
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="nbrtest" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="postifcase" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="importedCase" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="death" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="recovered" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="date" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "nombreTest",
    "nombreCasPositif",
    "nombreCasNegatif",
    "nombreDeces",
    "nombreGueris",
    "date"
})
@XmlRootElement(name = "getCovid19InfoResponse")
public class GetCovid19InfoResponse {

    @XmlElement(required = true)
    protected Integer nombreTest;
    @XmlElement(required = true)
    protected Integer nombreCasPositif;
    @XmlElement(required = true)
    protected Integer nombreCasNegatif;
    @XmlElement(required = true)
    protected Integer nombreDeces;
    @XmlElement(required = true)
    protected Integer nombreGueris;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar date;

    /**
     * Obtient la valeur de la propriété nbrtest.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public Integer getNombreTest() {
        return nombreTest;
    }

    public void setNombreTest(Integer nombreTest) {
        this.nombreTest = nombreTest;
    }

    public Integer getNombreCasPositif() {
        return nombreCasPositif;
    }

    public void setNombreCasPositif(Integer nombreCasPositif) {
        this.nombreCasPositif = nombreCasPositif;
    }

    public Integer getNombreCasNegatif() {
        return nombreCasNegatif;
    }

    public void setNombreCasNegatif(Integer nombreCasNegatif) {
        this.nombreCasNegatif = nombreCasNegatif;
    }

    public Integer getNombreDeces() {
        return nombreDeces;
    }

    public void setNombreDeces(Integer nombreDeces) {
        this.nombreDeces = nombreDeces;
    }

    public Integer getNombreGueris() {
        return nombreGueris;
    }

    public void setNombreGueris(Integer nombreGueris) {
        this.nombreGueris = nombreGueris;
    }

/**
     * Définit la valeur de la propriété recovered.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */

    /**
     * Obtient la valeur de la propriété date.
     *
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public XMLGregorianCalendar getDate() {
        return date;
    }

    /**
     * Définit la valeur de la propriété date.
     *
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *
     */
    public void setDate(XMLGregorianCalendar value) {
        this.date = value;
    }

}
