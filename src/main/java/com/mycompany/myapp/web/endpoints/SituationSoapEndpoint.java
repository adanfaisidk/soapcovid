package com.mycompany.myapp.web.endpoints;

import com.mycompany.myapp.domain.SituationCovid;
import com.mycompany.myapp.repository.SituationCovidRepository;
import com.mycompany.myapp.wsdl.GetCovid19InfoRequest;
import com.mycompany.myapp.wsdl.GetCovid19InfoResponse;
import com.mycompany.myapp.wsdl.GetInfoRequest;
import com.mycompany.myapp.wsdl.GetInfoResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;


import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.time.LocalDate;
import java.util.GregorianCalendar;
import java.util.Optional;

@Endpoint
public class SituationSoapEndpoint {

    @Autowired
    SituationCovidRepository situationCovidRepository;
    RestTemplate restTemplate = new RestTemplate();
    String url = "http://localhost:9500/api/situation-covids";
    String accessToken = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJaaUxwWFpwNUNmM2tsTHZ2NElqUFJ0aTZEV05yMzltSzBiUFhCbGRJUFFjIn0.eyJleHAiOjE2MTYyMzY4ODgsImlhdCI6MTYxNjIwMDg4OCwianRpIjoiYWRmNWMzMDAtOWM0Zi00NjQyLTgzYjgtMjA1ZTYyMzEyYTJjIiwiaXNzIjoiaHR0cDovL2xvY2FsaG9zdDo5MDgwL2F1dGgvcmVhbG1zL2poaXBzdGVyIiwiYXVkIjoiYWNjb3VudCIsInN1YiI6IjRjOTczODk2LTU3NjEtNDFmYy04MjE3LTA3YzVkMTNhMDA0YiIsInR5cCI6IkJlYXJlciIsImF6cCI6ImpoaXBzdGVyLXJlZ2lzdHJ5Iiwic2Vzc2lvbl9zdGF0ZSI6IjAyMWU1MTkwLTc3YjEtNDI2My1hOGM3LTI1NWQ1Njc5NTQwZCIsImFjciI6IjEiLCJhbGxvd2VkLW9yaWdpbnMiOlsiaHR0cDovLzEyNy4wLjAuMTo4NzYxIiwiaHR0cDovL2xvY2FsaG9zdDo4NzYxIl0sInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJST0xFX1VTRVIiLCJvZmZsaW5lX2FjY2VzcyIsIlJPTEVfQURNSU4iLCJ1bWFfYXV0aG9yaXphdGlvbiJdfSwicmVzb3VyY2VfYWNjZXNzIjp7ImFjY291bnQiOnsicm9sZXMiOlsibWFuYWdlLWFjY291bnQiLCJtYW5hZ2UtYWNjb3VudC1saW5rcyIsInZpZXctcHJvZmlsZSJdfX0sInNjb3BlIjoib3BlbmlkIGpoaXBzdGVyIGVtYWlsIHByb2ZpbGUiLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwicm9sZXMiOlsiUk9MRV9VU0VSIiwib2ZmbGluZV9hY2Nlc3MiLCJST0xFX0FETUlOIiwidW1hX2F1dGhvcml6YXRpb24iXSwibmFtZSI6IkFkbWluIEFkbWluaXN0cmF0b3IiLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJhZG1pbiIsImdpdmVuX25hbWUiOiJBZG1pbiIsImZhbWlseV9uYW1lIjoiQWRtaW5pc3RyYXRvciIsImVtYWlsIjoiYWRtaW5AbG9jYWxob3N0In0.mXk1_6p4sNi7UlS6yLY7K7yCNFq5JF8HCryW21KwTRdocXZUJn3WzZrkJJsA9qcYfOb8krhuNPyRuP8l8J3-1w3CewmsXKhVrIoDLt39nvVC_yBv2W_6okiA8hEqWZLAESCE7087TIcaTAS5kcnJ7KRgmDi3LCBPpQW1JS9cqqEFhwr43TsGO-kcOM-3O1y-ykAOWKh33UY1rUZk8Wd92Rm_VO99sxlGjdbJZpc8uDELuZwuHK-wrpml7OtjwlwzQLbf3uzkO97i8wcDtUtzZVCNX8eh7U2pW0WbyAC7Hg-pSlvEfetIkhGEsHCFVY70LhS-kn7jTudPVeydbfKOAQ";
    DatatypeFactory datatypeFactory;

    {
        try {
            datatypeFactory = DatatypeFactory.newInstance();
        } catch (DatatypeConfigurationException e) {
            e.printStackTrace();
        }
    }

    GregorianCalendar calendarExpected = new GregorianCalendar(2018, 6, 28);
    XMLGregorianCalendar expectedXMLGregorianCalendar = datatypeFactory
        .newXMLGregorianCalendar(calendarExpected);

    LocalDate localDate = LocalDate.of(2019, 4, 25);

        XMLGregorianCalendar xmlGregorianCalendar =
            datatypeFactory.newXMLGregorianCalendar(localDate.toString());


    @PayloadRoot(namespace = "http://ws.groupeisi.com",localPart = "getInfoRequest")
    public @ResponsePayload
    GetInfoResponse getInfoRequest (@RequestPayload GetInfoRequest request) {
        GetInfoResponse response= new GetInfoResponse();
        response.setOutput("Bienvenu sur Votre plateforme! "+ request.getInput());
        return response;
    }

    @PayloadRoot(namespace = "http://ws.groupeisi.com",localPart = "getCovid19InfoRequest")
    public @ResponsePayload
    GetCovid19InfoResponse getCovid19InfoRequest (@RequestPayload GetCovid19InfoRequest request) throws DatatypeConfigurationException {
        GetCovid19InfoResponse situationResponse= new GetCovid19InfoResponse();
        long bb = Long.parseLong(request.getInput());
        //SituationCovid SituationCovid = SituationCovidRepository.getCovidInfoById((long)11);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", "Bearer "+accessToken);
        HttpEntity<SituationCovid[]> entity = new HttpEntity<SituationCovid[]>(headers);

        ResponseEntity<SituationCovid[]> response = restTemplate.exchange(url, //
            HttpMethod.GET, entity, SituationCovid[].class);

        HttpStatus statusCode = response.getStatusCode();

        // Status Code: 200
        if (statusCode == HttpStatus.OK) {
            // Response Body Data
            SituationCovid[] list = response.getBody();

            if (list != null) {
                for (SituationCovid  st : list) {
                    System.out.println("Covid: " + st.getNombreTest() + " - " + st.getNombreGueris());
                    XMLGregorianCalendar dateFormat =
                        datatypeFactory.newXMLGregorianCalendar(st.getDate().toString());
                    situationResponse.setNombreCasPositif(st.getNombreCasPositif());
                    situationResponse.setNombreCasNegatif(st.getNombreCasNegatif());
                    situationResponse.setNombreTest(st.getNombreTest());
                    situationResponse.setNombreGueris(st.getNombreGueris());
                    situationResponse.setNombreDeces(st.getNombreDeces());
                    situationResponse.setDate(dateFormat);
                }
            }
        }

        return situationResponse;
    }

    /*public Date StringToDate(String s){

        Date result = null;
        try{
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            result  = dateFormat.parse(s);
        }

        catch(ParseException e){
            e.printStackTrace();

        }
        return result ;
    }*/
}
