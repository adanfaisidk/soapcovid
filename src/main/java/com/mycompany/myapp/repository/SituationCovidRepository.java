package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.SituationCovid;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the SituationCovid entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SituationCovidRepository extends JpaRepository<SituationCovid, Long> {
    @Query("select s  from SituationCovid s where s.id=:id")
    SituationCovid getSituationCovidById(Long id);
}
